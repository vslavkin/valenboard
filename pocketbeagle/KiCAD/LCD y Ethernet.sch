EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 6 9
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	2500 3300 2500 3500
Wire Wire Line
	2300 3500 2300 3300
Wire Wire Line
	1100 3500 1100 3300
Wire Wire Line
	2200 3500 2200 3400
Wire Wire Line
	2800 3500 2800 3400
Wire Wire Line
	2700 3500 2700 3400
Wire Wire Line
	2600 3500 2600 3400
Wire Wire Line
	1000 3500 1000 3400
Wire Wire Line
	1200 3500 1200 3400
Wire Wire Line
	1300 3500 1300 3400
Wire Wire Line
	1600 3500 1600 3400
Wire Wire Line
	1700 3500 1700 3400
Wire Wire Line
	1800 3500 1800 3400
Wire Wire Line
	2000 3500 2000 3400
Wire Wire Line
	2100 3400 2100 3500
Wire Wire Line
	1000 5500 1000 3900
Wire Wire Line
	3000 5500 1000 5500
Wire Wire Line
	1300 5200 1300 3900
Wire Wire Line
	3000 5200 1300 5200
Wire Wire Line
	1200 5300 1200 3900
Wire Wire Line
	3000 5300 1200 5300
Wire Wire Line
	1100 5400 1100 3900
Wire Wire Line
	3000 5400 1100 5400
Wire Wire Line
	1700 4900 1700 3900
Wire Wire Line
	3000 4900 1700 4900
Wire Wire Line
	1600 5000 1600 3900
Wire Wire Line
	3000 5000 1600 5000
Wire Wire Line
	1500 5100 1500 3900
Wire Wire Line
	3000 5100 1500 5100
Wire Wire Line
	3000 4600 2100 4600
Wire Wire Line
	2100 4600 2100 3900
Wire Wire Line
	2000 4700 2000 3900
Wire Wire Line
	3000 4700 2000 4700
Wire Wire Line
	1800 4800 1800 3900
Wire Wire Line
	3000 4800 1800 4800
Wire Wire Line
	3000 4300 2500 4300
Wire Wire Line
	2500 4300 2500 3900
Wire Wire Line
	3000 4400 2300 4400
Wire Wire Line
	2300 4400 2300 3900
Wire Wire Line
	3000 4500 2200 4500
Wire Wire Line
	2200 4500 2200 3900
Wire Wire Line
	3000 4000 2800 4000
Wire Wire Line
	2800 4000 2800 3900
Wire Wire Line
	3000 4100 2700 4100
Wire Wire Line
	2700 4100 2700 3900
Wire Wire Line
	3000 4200 2600 4200
Wire Wire Line
	2600 4200 2600 3900
$Comp
L ValenBoard-rescue:VDD_3V3B-PocketBeagle #SUPPLY?
U 1 1 602FBA0F
P 2500 3200
AR Path="/602FBA0F" Ref="#SUPPLY?"  Part="1" 
AR Path="/602698DD/602FBA0F" Ref="#SUPPLY?"  Part="1" 
F 0 "#SUPPLY?" V 2460 3020 40  0001 L BNN
F 1 "VDD_3V3B" V 2500 3350 31  0000 L BNN
F 2 "" H 2500 3200 60  0001 C CNN
F 3 "" H 2500 3200 60  0001 C CNN
	1    2500 3200
	1    0    0    -1  
$EndComp
$Comp
L ValenBoard-rescue:VDD_3V3B-PocketBeagle #SUPPLY?
U 1 1 602FBA15
P 1100 3200
AR Path="/602FBA15" Ref="#SUPPLY?"  Part="1" 
AR Path="/602698DD/602FBA15" Ref="#SUPPLY?"  Part="1" 
F 0 "#SUPPLY?" H 1100 3360 40  0001 L BNN
F 1 "VDD_3V3B" V 1100 3350 31  0000 L BNN
F 2 "" H 1100 3200 60  0001 C CNN
F 3 "" H 1100 3200 60  0001 C CNN
	1    1100 3200
	1    0    0    -1  
$EndComp
$Comp
L ValenBoard-rescue:VDD_3V3B-PocketBeagle #SUPPLY?
U 1 1 602FBA1B
P 2300 3200
AR Path="/602FBA1B" Ref="#SUPPLY?"  Part="1" 
AR Path="/602698DD/602FBA1B" Ref="#SUPPLY?"  Part="1" 
F 0 "#SUPPLY?" V 2339 3020 40  0001 L BNN
F 1 "VDD_3V3B" V 2300 3350 31  0000 L BNN
F 2 "" H 2300 3200 60  0001 C CNN
F 3 "" H 2300 3200 60  0001 C CNN
	1    2300 3200
	1    0    0    -1  
$EndComp
$Comp
L ValenBoard-rescue:RESISTOR.NETWORK-_4_-PocketBeagle R?
U 1 1 602FBA27
P 2200 3700
AR Path="/602FBA27" Ref="R?"  Part="1" 
AR Path="/602698DD/602FBA27" Ref="R?"  Part="1" 
F 0 "R?" V 1975 4050 31  0000 R TNN
F 1 "100k" V 1925 4050 31  0000 R TNN
F 2 "PocketBeagle.pretty:RN0804" H 2190 3490 65  0001 L TNN
F 3 "" H 2200 3700 60  0001 C CNN
	1    2200 3700
	0    -1   -1   0   
$EndComp
$Comp
L ValenBoard-rescue:RESISTOR.NETWORK-_4_-PocketBeagle R?
U 1 1 602FBA2D
P 1700 3700
AR Path="/602FBA2D" Ref="R?"  Part="1" 
AR Path="/602698DD/602FBA2D" Ref="R?"  Part="1" 
F 0 "R?" V 1475 4050 31  0000 R TNN
F 1 "100k" V 1425 4050 31  0000 R TNN
F 2 "PocketBeagle.pretty:RN0804" H 1690 3490 65  0001 L TNN
F 3 "" H 1700 3700 60  0001 C CNN
	1    1700 3700
	0    -1   -1   0   
$EndComp
$Comp
L ValenBoard-rescue:RESISTOR.NETWORK-_4_-PocketBeagle R?
U 1 1 602FBA33
P 1200 3700
AR Path="/602FBA33" Ref="R?"  Part="1" 
AR Path="/602698DD/602FBA33" Ref="R?"  Part="1" 
F 0 "R?" V 975 4050 31  0000 R TNN
F 1 "100k" V 925 4050 31  0000 R TNN
F 2 "PocketBeagle.pretty:RN0804" H 1190 3490 65  0001 L TNN
F 3 "" H 1200 3700 60  0001 C CNN
	1    1200 3700
	0    -1   -1   0   
$EndComp
$Comp
L ValenBoard-rescue:GND-PocketBeagle #GND?
U 1 1 602FBA39
P 2800 3300
AR Path="/602FBA39" Ref="#GND?"  Part="1" 
AR Path="/602698DD/602FBA39" Ref="#GND?"  Part="1" 
F 0 "#GND?" V 2819 3380 40  0001 R TNN
F 1 "GND" V 2800 3250 31  0000 R TNN
F 2 "" H 2800 3300 60  0001 C CNN
F 3 "" H 2800 3300 60  0001 C CNN
	1    2800 3300
	-1   0    0    1   
$EndComp
$Comp
L ValenBoard-rescue:GND-PocketBeagle #GND?
U 1 1 602FBA3F
P 1200 3300
AR Path="/602FBA3F" Ref="#GND?"  Part="1" 
AR Path="/602698DD/602FBA3F" Ref="#GND?"  Part="1" 
F 0 "#GND?" V 1219 3380 40  0001 R TNN
F 1 "GND" V 1200 3250 31  0000 R TNN
F 2 "" H 1200 3300 60  0001 C CNN
F 3 "" H 1200 3300 60  0001 C CNN
	1    1200 3300
	-1   0    0    1   
$EndComp
$Comp
L ValenBoard-rescue:GND-PocketBeagle #GND?
U 1 1 602FBA45
P 1300 3300
AR Path="/602FBA45" Ref="#GND?"  Part="1" 
AR Path="/602698DD/602FBA45" Ref="#GND?"  Part="1" 
F 0 "#GND?" V 1319 3380 40  0001 R TNN
F 1 "GND" V 1300 3250 31  0000 R TNN
F 2 "" H 1300 3300 60  0001 C CNN
F 3 "" H 1300 3300 60  0001 C CNN
	1    1300 3300
	-1   0    0    1   
$EndComp
$Comp
L ValenBoard-rescue:GND-PocketBeagle #GND?
U 1 1 602FBA4B
P 1500 3300
AR Path="/602FBA4B" Ref="#GND?"  Part="1" 
AR Path="/602698DD/602FBA4B" Ref="#GND?"  Part="1" 
F 0 "#GND?" V 1519 3380 40  0001 R TNN
F 1 "GND" V 1500 3250 31  0000 R TNN
F 2 "" H 1500 3300 60  0001 C CNN
F 3 "" H 1500 3300 60  0001 C CNN
	1    1500 3300
	-1   0    0    1   
$EndComp
$Comp
L ValenBoard-rescue:GND-PocketBeagle #GND?
U 1 1 602FBA51
P 1600 3300
AR Path="/602FBA51" Ref="#GND?"  Part="1" 
AR Path="/602698DD/602FBA51" Ref="#GND?"  Part="1" 
F 0 "#GND?" V 1619 3380 40  0001 R TNN
F 1 "GND" V 1600 3250 31  0000 R TNN
F 2 "" H 1600 3300 60  0001 C CNN
F 3 "" H 1600 3300 60  0001 C CNN
	1    1600 3300
	-1   0    0    1   
$EndComp
$Comp
L ValenBoard-rescue:GND-PocketBeagle #GND?
U 1 1 602FBA57
P 1700 3300
AR Path="/602FBA57" Ref="#GND?"  Part="1" 
AR Path="/602698DD/602FBA57" Ref="#GND?"  Part="1" 
F 0 "#GND?" V 1719 3380 40  0001 R TNN
F 1 "GND" V 1700 3250 31  0000 R TNN
F 2 "" H 1700 3300 60  0001 C CNN
F 3 "" H 1700 3300 60  0001 C CNN
	1    1700 3300
	-1   0    0    1   
$EndComp
$Comp
L ValenBoard-rescue:GND-PocketBeagle #GND?
U 1 1 602FBA5D
P 1800 3300
AR Path="/602FBA5D" Ref="#GND?"  Part="1" 
AR Path="/602698DD/602FBA5D" Ref="#GND?"  Part="1" 
F 0 "#GND?" V 1819 3380 40  0001 R TNN
F 1 "GND" V 1800 3250 31  0000 R TNN
F 2 "" H 1800 3300 60  0001 C CNN
F 3 "" H 1800 3300 60  0001 C CNN
	1    1800 3300
	-1   0    0    1   
$EndComp
$Comp
L ValenBoard-rescue:GND-PocketBeagle #GND?
U 1 1 602FBA63
P 2000 3300
AR Path="/602FBA63" Ref="#GND?"  Part="1" 
AR Path="/602698DD/602FBA63" Ref="#GND?"  Part="1" 
F 0 "#GND?" V 2019 3380 40  0001 R TNN
F 1 "GND" V 2000 3250 31  0000 R TNN
F 2 "" H 2000 3300 60  0001 C CNN
F 3 "" H 2000 3300 60  0001 C CNN
	1    2000 3300
	-1   0    0    1   
$EndComp
$Comp
L ValenBoard-rescue:GND-PocketBeagle #GND?
U 1 1 602FBA69
P 2100 3300
AR Path="/602FBA69" Ref="#GND?"  Part="1" 
AR Path="/602698DD/602FBA69" Ref="#GND?"  Part="1" 
F 0 "#GND?" V 2119 3380 40  0001 R TNN
F 1 "GND" V 2100 3250 31  0000 R TNN
F 2 "" H 2100 3300 60  0001 C CNN
F 3 "" H 2100 3300 60  0001 C CNN
	1    2100 3300
	-1   0    0    1   
$EndComp
$Comp
L ValenBoard-rescue:GND-PocketBeagle #GND?
U 1 1 602FBA6F
P 1000 3300
AR Path="/602FBA6F" Ref="#GND?"  Part="1" 
AR Path="/602698DD/602FBA6F" Ref="#GND?"  Part="1" 
F 0 "#GND?" V 1019 3380 40  0001 R TNN
F 1 "GND" V 1000 3250 31  0000 R TNN
F 2 "" H 1000 3300 60  0001 C CNN
F 3 "" H 1000 3300 60  0001 C CNN
	1    1000 3300
	-1   0    0    1   
$EndComp
$Comp
L ValenBoard-rescue:GND-PocketBeagle #GND?
U 1 1 602FBA75
P 2700 3300
AR Path="/602FBA75" Ref="#GND?"  Part="1" 
AR Path="/602698DD/602FBA75" Ref="#GND?"  Part="1" 
F 0 "#GND?" V 2719 3380 40  0001 R TNN
F 1 "GND" V 2700 3250 31  0000 R TNN
F 2 "" H 2700 3300 60  0001 C CNN
F 3 "" H 2700 3300 60  0001 C CNN
	1    2700 3300
	-1   0    0    1   
$EndComp
$Comp
L ValenBoard-rescue:GND-PocketBeagle #GND?
U 1 1 602FBA7B
P 2600 3300
AR Path="/602FBA7B" Ref="#GND?"  Part="1" 
AR Path="/602698DD/602FBA7B" Ref="#GND?"  Part="1" 
F 0 "#GND?" V 2619 3380 40  0001 R TNN
F 1 "GND" V 2600 3250 31  0000 R TNN
F 2 "" H 2600 3300 60  0001 C CNN
F 3 "" H 2600 3300 60  0001 C CNN
	1    2600 3300
	-1   0    0    1   
$EndComp
$Comp
L ValenBoard-rescue:GND-PocketBeagle #GND?
U 1 1 602FBA81
P 2200 3300
AR Path="/602FBA81" Ref="#GND?"  Part="1" 
AR Path="/602698DD/602FBA81" Ref="#GND?"  Part="1" 
F 0 "#GND?" V 2219 3380 40  0001 R TNN
F 1 "GND" V 2200 3250 31  0000 R TNN
F 2 "" H 2200 3300 60  0001 C CNN
F 3 "" H 2200 3300 60  0001 C CNN
	1    2200 3300
	-1   0    0    1   
$EndComp
Text Notes 2900 4000 0    70   ~ 0
0
Text Notes 2800 4100 0    70   ~ 0
0
Text Notes 2700 4200 0    70   ~ 0
0
Text Notes 2600 4300 0    70   ~ 0
1
Text Notes 2500 4400 0    70   ~ 0
1
Text Notes 2400 4500 0    70   ~ 0
0
Text Notes 2300 4600 0    70   ~ 0
0
Text Notes 2200 4700 0    70   ~ 0
0
Text Notes 2100 4800 0    70   ~ 0
0
Text Notes 2000 4900 0    70   ~ 0
0
Text Notes 1900 5000 0    70   ~ 0
0
Text Notes 1800 5100 0    70   ~ 0
0
Text Notes 1700 5200 0    70   ~ 0
0
Text Notes 1600 5300 0    70   ~ 0
0
Text Notes 1500 5400 0    70   ~ 0
1
Text Notes 1400 5500 0    70   ~ 0
0
Text Notes 700  2200 0    254  ~ 0
BeagleBone Black\nConfiguration
$Comp
L ValenBoard-rescue:OSD3358-512M-BSM-PocketBeagle U?
U 4 1 6033F6C7
P 6650 5300
F 0 "U?" H 6650 8100 70  0000 L BNN
F 1 "OSD3358-512M-BSM" H 6650 5200 70  0000 L BNN
F 2 "PocketBeagle.pretty:OSD335X-BGA-256" H 6640 5090 65  0001 L TNN
F 3 "" H 6650 5300 60  0001 C CNN
	4    6650 5300
	1    0    0    -1  
$EndComp
NoConn ~ 6450 2800
NoConn ~ 6450 2900
NoConn ~ 6450 3000
NoConn ~ 6450 3100
NoConn ~ 6450 3200
NoConn ~ 6450 3300
NoConn ~ 6450 3400
NoConn ~ 6450 3500
NoConn ~ 6450 3700
NoConn ~ 6450 3800
NoConn ~ 6450 3900
NoConn ~ 6450 4000
NoConn ~ 6450 4100
NoConn ~ 6450 4200
NoConn ~ 6450 4300
NoConn ~ 6450 4500
NoConn ~ 6450 4600
NoConn ~ 6450 4700
Wire Wire Line
	8850 2800 9000 2800
Wire Wire Line
	8850 4500 9000 4500
Wire Wire Line
	8850 4600 9000 4600
Wire Wire Line
	8850 4700 9000 4700
Wire Wire Line
	8850 4800 9000 4800
Wire Wire Line
	8850 2900 9000 2900
Wire Wire Line
	8850 3000 9000 3000
Wire Wire Line
	8850 3100 9000 3100
Wire Wire Line
	8850 3200 9000 3200
Wire Wire Line
	8850 3300 9000 3300
Wire Wire Line
	8850 3400 9000 3400
Wire Wire Line
	8850 3500 9000 3500
Wire Wire Line
	8850 3600 9000 3600
Wire Wire Line
	8850 3700 9000 3700
Wire Wire Line
	8850 3800 9000 3800
Wire Wire Line
	8850 3900 9000 3900
Wire Wire Line
	8850 4000 9000 4000
Wire Wire Line
	8850 4100 9000 4100
Wire Wire Line
	8850 4200 9000 4200
Wire Wire Line
	8850 4300 9000 4300
Text HLabel 3000 4000 2    47   Input ~ 0
(U1.G3)LCD.D0.B3
Text HLabel 3000 4100 2    47   Input ~ 0
(U1.G2)LCD.D1.B4
Text HLabel 3000 4200 2    47   Input ~ 0
(U1.G1)LCD.D2.B5
Text HLabel 3000 4300 2    47   Input ~ 0
(U1.H3)LCD.D3.B6
Text HLabel 3000 4400 2    47   Input ~ 0
(U1.H2)LCD.D4.B7
Text HLabel 3000 4500 2    47   Input ~ 0
(U1.H1)LCD.D5.G2
Text HLabel 3000 4600 2    47   Input ~ 0
(U1.J3)LCD.D6.G3
Text HLabel 3000 4700 2    47   Input ~ 0
(U1.J2)LCD.D7.G4
Text HLabel 3000 4800 2    47   Input ~ 0
(U1.J1)LCD.D8.G5
Text HLabel 3000 4900 2    47   Input ~ 0
(U1.K3)LCD.D9.G6
Text HLabel 3000 5000 2    47   Input ~ 0
(U1.K2)LCD.D10.G7
Text HLabel 3000 5100 2    47   Input ~ 0
(U1.K1)LCD.D11.R3
Text HLabel 3000 5200 2    47   Input ~ 0
(U1.L3)LCD.D12.R4
Text HLabel 3000 5300 2    47   Input ~ 0
(U1.L2)LCD.D13.R5
Text HLabel 3000 5400 2    47   Input ~ 0
(U1.L1)LCD.D14.R6
Text HLabel 3000 5500 2    47   Input ~ 0
(U1.M3)LCD.D15.R7
Text HLabel 9000 2800 2    50   Input ~ 0
(U1.G3)LCD.D0.B3
Text HLabel 9000 2900 2    50   Input ~ 0
(U1.G2)LCD.D1.B4
Text HLabel 9000 3000 2    50   Input ~ 0
(U1.G1)LCD.D2.B5
Text HLabel 9000 3100 2    50   Input ~ 0
(U1.H3)LCD.D3.B6
Text HLabel 9000 3200 2    50   Input ~ 0
(U1.H2)LCD.D4.B7
Text HLabel 9000 3300 2    50   Input ~ 0
(U1.H1)LCD.D5.G2
Text HLabel 9000 3400 2    50   Input ~ 0
(U1.J3)LCD.D6.G3
Text HLabel 9000 3500 2    50   Input ~ 0
(U1.J2)LCD.D7.G4
Text HLabel 9000 3600 2    50   Input ~ 0
(U1.J1)LCD.D8.G5
Text HLabel 9000 3700 2    50   Input ~ 0
(U1.K3)LCD.D9.G6
Text HLabel 9000 3800 2    50   Input ~ 0
(U1.K2)LCD.D10.G7
Text HLabel 9000 3900 2    50   Input ~ 0
(U1.K1)LCD.D11.R3
Text HLabel 9000 4000 2    50   Input ~ 0
(U1.L3)LCD.D12.R4
Text HLabel 9000 4100 2    50   Input ~ 0
(U1.L2)LCD.D13.R5
Text HLabel 9000 4200 2    50   Input ~ 0
(U1.L1)LCD.D14.R6
Text HLabel 9000 4300 2    50   Input ~ 0
(U1.M3)LCD.D15.R7
Text HLabel 9000 4500 2    50   Input ~ 0
(U1.F1)
Text HLabel 9000 4600 2    50   Input ~ 0
(U1.F3)AIN5~3.3V
Text HLabel 9000 4700 2    50   Input ~ 0
(U1.F2)AIN6~3.3V
Text HLabel 9000 4800 2    50   Input ~ 0
(U1.E1)
$Comp
L ValenBoard-rescue:RESISTOR.NETWORK-_4_-PocketBeagle R?
U 1 1 602FBA21
P 2600 3700
AR Path="/602FBA21" Ref="R?"  Part="1" 
AR Path="/602698DD/602FBA21" Ref="R?"  Part="1" 
F 0 "R?" V 2825 3451 31  0000 L BNN
F 1 "100k" V 2875 3451 31  0000 L BNN
F 2 "PocketBeagle.pretty:RN0804" H 2590 3490 65  0001 L TNN
F 3 "" H 2600 3700 60  0001 C CNN
	1    2600 3700
	0    1    1    0   
$EndComp
Text Notes 4070 5530 0    67   ~ 0
<-- SYS_BOOT0\n<-- SYS_BOOT1\n<-- SYS_BOOT2\n<-- SYS_BOOT3\n<-- SYS_BOOT4\n<-- SYS_BOOT5\n<-- SYS_BOOT6\n<-- SYS_BOOT7\n<-- SYS_BOOT8\n<-- SYS_BOOT9\n<-- SYS_BOOT10\n<-- SYS_BOOT11\n<-- SYS_BOOT12\n<-- SYS_BOOT13\n<-- SYS_BOOT14\n<-- SYS_BOOT15
Wire Wire Line
	1500 3500 1500 3400
$EndSCHEMATC
