EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Notes Line
	6950 5300 3750 5300
Text Label 5850 3400 2    10   ~ 0
N$6
Text Label 5550 3000 0    10   ~ 0
N$6
Text Label 5850 3500 2    10   ~ 0
N$5
Text Label 5550 3400 0    10   ~ 0
N$5
Text Label 5850 3700 2    10   ~ 0
N$2
Text Label 5550 4200 0    10   ~ 0
N$2
Text Label 5850 3600 2    10   ~ 0
N$1
Text Label 5550 3800 0    10   ~ 0
N$1
Text GLabel 6250 3700 2    10   UnSpc ~ 0
GND
Text GLabel 6250 3600 2    10   UnSpc ~ 0
GND
Text GLabel 6250 3500 2    10   UnSpc ~ 0
GND
Text GLabel 6250 3400 2    10   UnSpc ~ 0
GND
Text GLabel 6350 3800 1    10   UnSpc ~ 0
GND
Text Label 5250 3400 2    10   ~ 0
(U1.T14)USR1
Text Label 5250 3800 2    10   ~ 0
(U1.R14)USR2
Text Label 5250 4200 2    10   ~ 0
(U1.P14)USR3
Text Label 5250 3000 2    10   ~ 0
(U1.P13)USR0
Wire Notes Line
	6950 1500 6950 5300
Wire Notes Line
	3750 1500 6950 1500
Wire Notes Line
	3750 5300 3750 1500
Wire Wire Line
	5750 3400 5850 3400
Wire Wire Line
	5750 3000 5750 3400
Wire Wire Line
	5550 3000 5750 3000
Wire Wire Line
	5650 3500 5850 3500
Wire Wire Line
	5650 3400 5650 3500
Wire Wire Line
	5550 3400 5650 3400
Wire Wire Line
	5750 3700 5850 3700
Wire Wire Line
	5750 4200 5750 3700
Wire Wire Line
	5550 4200 5750 4200
Wire Wire Line
	5650 3600 5850 3600
Wire Wire Line
	5650 3800 5650 3600
Wire Wire Line
	5550 3800 5650 3800
Connection ~ 6350 3700
Connection ~ 6350 3600
Connection ~ 6350 3500
Wire Wire Line
	6250 3700 6350 3700
Wire Wire Line
	6250 3600 6350 3600
Wire Wire Line
	6250 3500 6350 3500
Wire Wire Line
	6350 3400 6250 3400
Wire Wire Line
	6350 3700 6350 3800
Wire Wire Line
	6350 3600 6350 3700
Wire Wire Line
	6350 3500 6350 3600
Wire Wire Line
	6350 3400 6350 3500
Text Label 4350 3400 0    65   ~ 0
(U1.T14)USR1
Wire Wire Line
	4350 3400 5250 3400
Text Label 4350 4200 0    65   ~ 0
(U1.P14)USR3
Wire Wire Line
	4350 4200 5250 4200
Text Label 4350 3000 0    65   ~ 0
(U1.P13)USR0
Wire Wire Line
	4350 3000 5250 3000
$Comp
L PocketBeagle:LED-_INCH_-0603 USR?
U 1 1 5FDE0BEB
P 5350 4200
F 0 "USR?" V 5490 4020 70  0000 L BNN
F 1 "LTST-C191TBKT" V 5575 4020 70  0000 L BNN
F 2 "PocketBeagle.pretty:0603-LED" H 5340 3990 65  0001 L TNN
F 3 "" H 5350 4200 60  0001 C CNN
	1    5350 4200
	0    -1   -1   0   
$EndComp
$Comp
L PocketBeagle:LED-_INCH_-0603 USR?
U 1 1 5FDE0BF1
P 5350 3800
F 0 "USR?" V 5490 3620 70  0000 L BNN
F 1 "LTST-C191TBKT" V 5575 3620 70  0000 L BNN
F 2 "PocketBeagle.pretty:0603-LED" H 5340 3590 65  0001 L TNN
F 3 "" H 5350 3800 60  0001 C CNN
	1    5350 3800
	0    -1   -1   0   
$EndComp
$Comp
L PocketBeagle:LED-_INCH_-0603 USR?
U 1 1 5FDE0BF7
P 5350 3400
F 0 "USR?" V 5490 3220 70  0000 L BNN
F 1 "LTST-C191TBKT" V 5575 3220 70  0000 L BNN
F 2 "PocketBeagle.pretty:0603-LED" H 5340 3190 65  0001 L TNN
F 3 "" H 5350 3400 60  0001 C CNN
	1    5350 3400
	0    -1   -1   0   
$EndComp
$Comp
L PocketBeagle:LED-_INCH_-0603 USR?
U 1 1 5FDE0BFD
P 5350 3000
F 0 "USR?" V 5490 2820 70  0000 L BNN
F 1 "LTST-C191TBKT" V 5575 2820 70  0000 L BNN
F 2 "PocketBeagle.pretty:0603-LED" H 5340 2790 65  0001 L TNN
F 3 "" H 5350 3000 60  0001 C CNN
	1    5350 3000
	0    -1   -1   0   
$EndComp
$Comp
L PocketBeagle:RESISTOR.NETWORK-_4_ R?
U 1 1 5FDE0C03
P 6050 3600
F 0 "R?" H 5901 3975 40  0000 L BNN
F 1 "1k" H 5901 3925 40  0000 L BNN
F 2 "PocketBeagle.pretty:RN0804" H 6040 3390 65  0001 L TNN
F 3 "" H 6050 3600 60  0001 C CNN
	1    6050 3600
	1    0    0    -1  
$EndComp
$Comp
L PocketBeagle:GND #GND?
U 1 1 5FDE0C09
P 6350 3900
F 0 "#GND?" H 6250 3700 70  0001 L BNN
F 1 "GND" H 6250 3800 70  0000 L BNN
F 2 "" H 6350 3900 60  0001 C CNN
F 3 "" H 6350 3900 60  0001 C CNN
	1    6350 3900
	1    0    0    -1  
$EndComp
Text Notes 4450 2600 0    254  ~ 0
USER LEDs
Text HLabel 4350 3800 0    50   Input ~ 0
(U1.R14)USR2
Wire Wire Line
	4350 3800 5250 3800
$EndSCHEMATC
