EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 9 9
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L ValenBoard-rescue:PTR1B1_27-PocketBeagle TP?
U 1 1 602DF7E8
P 8100 5850
AR Path="/602DF7E8" Ref="TP?"  Part="1" 
AR Path="/602D1F90/602DF7E8" Ref="TP?"  Part="1" 
F 0 "TP?" V 8061 5600 70  0000 L BNN
F 1 "WP" V 8050 5400 65  0000 L TNN
F 2 "PocketBeagle.pretty:B1_27" H 8090 5640 65  0001 L TNN
F 3 "" H 8100 5850 60  0001 C CNN
	1    8100 5850
	0    -1   1    0   
$EndComp
Wire Wire Line
	4000 4250 3450 4250
Wire Wire Line
	3450 4350 4000 4350
Wire Wire Line
	3450 4650 4000 4650
Wire Wire Line
	3450 5250 4000 5250
Wire Wire Line
	4000 2450 3450 2450
Wire Wire Line
	3450 2250 4000 2250
Wire Wire Line
	3450 1650 4000 1650
Wire Wire Line
	4000 2750 3450 2750
Wire Wire Line
	4000 3050 3450 3050
Wire Wire Line
	3450 3750 4000 3750
Wire Wire Line
	3450 3650 4000 3650
Wire Wire Line
	3450 4550 4000 4550
Wire Wire Line
	3450 4450 4000 4450
Wire Wire Line
	3450 4750 4000 4750
Wire Wire Line
	4000 5350 3450 5350
Wire Wire Line
	4000 2350 3450 2350
Wire Wire Line
	3450 2150 4000 2150
Wire Wire Line
	3450 1550 4000 1550
Wire Wire Line
	4000 2850 3450 2850
Wire Wire Line
	4000 2950 3450 2950
Wire Wire Line
	3450 3450 4000 3450
Wire Wire Line
	3450 3550 4000 3550
Wire Wire Line
	3450 4850 4000 4850
Wire Wire Line
	3450 4950 4000 4950
Wire Wire Line
	3450 5450 4000 5450
Wire Wire Line
	3450 1750 4000 1750
Wire Wire Line
	3450 1850 4000 1850
Wire Wire Line
	4000 3150 3450 3150
Wire Wire Line
	3450 3950 4000 3950
Wire Wire Line
	3450 3850 4000 3850
Wire Wire Line
	6700 5850 8000 5850
Text Label 7000 5850 0    39   ~ 0
(U1.M2)EEPROM.WP
Wire Wire Line
	7150 3050 6700 3050
Wire Wire Line
	6700 4950 7150 4950
Wire Wire Line
	7150 5250 6700 5250
Wire Wire Line
	7150 5550 6700 5550
Wire Wire Line
	6700 1950 7150 1950
Wire Wire Line
	6700 2250 7150 2250
Wire Wire Line
	6700 2550 7150 2550
Wire Wire Line
	7150 3150 6700 3150
Wire Wire Line
	7150 5050 6700 5050
Wire Wire Line
	6700 5350 7150 5350
Wire Wire Line
	7150 3750 6700 3750
Wire Wire Line
	6700 2150 7150 2150
Wire Wire Line
	6700 2450 7150 2450
Wire Wire Line
	7150 2750 6700 2750
Wire Wire Line
	7150 3250 6700 3250
Wire Wire Line
	7150 5150 6700 5150
Wire Wire Line
	7150 5450 6700 5450
Wire Wire Line
	7150 1550 6700 1550
Wire Wire Line
	6700 2350 7150 2350
Wire Wire Line
	6700 2650 7150 2650
Wire Wire Line
	7150 2850 6700 2850
NoConn ~ 6700 1750
NoConn ~ 6700 1850
NoConn ~ 6700 2050
NoConn ~ 6700 3450
NoConn ~ 6700 3550
NoConn ~ 6700 3650
NoConn ~ 6700 3950
NoConn ~ 6700 4050
NoConn ~ 6700 4150
NoConn ~ 6700 4250
NoConn ~ 6700 4350
NoConn ~ 6700 4450
NoConn ~ 6700 4550
NoConn ~ 6700 4650
NoConn ~ 6700 4850
NoConn ~ 6700 5750
NoConn ~ 6700 5950
NoConn ~ 6700 6050
Text HLabel 3450 1550 0    50   Input ~ 0
(U1.B12)UART0.TX
Text HLabel 3450 1650 0    50   Input ~ 0
(U1.A12)UART0.RX
Text HLabel 3450 1750 0    50   Input ~ 0
(U1.C12)SPI1.MISO
Text HLabel 3450 1850 0    50   Input ~ 0
(U1.C13)SPI1.MOSI
Text HLabel 3450 2150 0    50   Input ~ 0
(U1.B11)I2C1.SCL
Text HLabel 3450 2250 0    50   Input ~ 0
(U1.A11)I2C1.SDA
Text HLabel 3450 2350 0    50   Input ~ 0
(U1.B10)I2C2.SDA
Text HLabel 3450 2450 0    50   Input ~ 0
(U1.A10)I2C2.SCL
Text HLabel 3450 2750 0    50   Input ~ 0
(U1.A13)SPI0.CLK
Text HLabel 3450 2850 0    50   Input ~ 0
(U1.B13)SPI0.MISO
Text HLabel 3450 2950 0    50   Input ~ 0
(U1.B14)SPI0.MOSI
Text HLabel 3450 3050 0    50   Input ~ 0
(U1.A14)SPI0.CS
Text HLabel 3450 3150 0    50   Input ~ 0
(U1.C14)MMC0.CD
Text HLabel 3450 3450 0    50   Input ~ 0
(U1.B15)MMC0.CLK
Text HLabel 3450 3550 0    50   Input ~ 0
(U1.B16)MMC0.CMD
Text HLabel 3450 3650 0    50   Input ~ 0
(U1.A16)MMC0.D0
Text HLabel 3450 3750 0    50   Input ~ 0
(U1.A15)MMC0.D1
Text HLabel 3450 3850 0    50   Input ~ 0
(U1.C16)MMC0.D2
Text HLabel 3450 3950 0    50   Input ~ 0
(U1.C15)MMC0.D3
Text HLabel 3450 4250 0    50   Input ~ 0
(U1.A1)PWM0A
Text HLabel 3450 4350 0    50   Input ~ 0
(U1.A2)PRU-0.1
Text HLabel 3450 4450 0    50   Input ~ 0
(U1.B2)PRU-0.2
Text HLabel 3450 4550 0    50   Input ~ 0
(U1.B1)PRU-0.3
Text HLabel 3450 4650 0    50   Input ~ 0
(U1.A3)PRU-0.4
Text HLabel 3450 4750 0    50   Input ~ 0
(U1.B3)PRU-0.5
Text HLabel 3450 4850 0    50   Input ~ 0
(U1.C3)PRU-0.6
Text HLabel 3450 4950 0    50   Input ~ 0
(U1.C4)PRU-0.7
Text HLabel 3450 5250 0    50   Input ~ 0
(U1.A4)SPI1.CS
Text HLabel 3450 5350 0    50   Input ~ 0
(U1.B4)PRU-0.16
Text HLabel 3450 5450 0    50   Input ~ 0
(U1.C5)SPI1.CLK
Text HLabel 7150 1550 2    50   Input ~ 0
(U1.T7)
Text HLabel 7150 1950 2    50   Input ~ 0
(U1.P12)PWM1A
Text HLabel 7150 2150 2    50   Input ~ 0
(U1.R13)
Text HLabel 7150 2250 2    50   Input ~ 0
(U1.P13)USR0
Text HLabel 7150 2350 2    50   Input ~ 0
(U1.T14)USR1
Text HLabel 7150 2450 2    50   Input ~ 0
(U1.R14)USR2
Text HLabel 7150 2550 2    50   Input ~ 0
(U1.P14)USR3
Text HLabel 7150 2650 2    50   Input ~ 0
(U1.T15)
Text HLabel 7150 2750 2    50   Input ~ 0
(U1.R15)
Text HLabel 7150 2850 2    50   Input ~ 0
(U1.T16)
Text HLabel 7150 3050 2    50   Input ~ 0
(U1.N14)
Text HLabel 7150 3150 2    50   Input ~ 0
(U1.P15)UART4.RX
Text HLabel 7150 3250 2    50   Input ~ 0
(U1.R16)UART4.TX
Text HLabel 7150 3750 2    50   Input ~ 0
(U1.R7)
Text HLabel 7150 4950 2    50   Input ~ 0
(U1.P5)GPIO-0.23
Text HLabel 7150 5050 2    50   Input ~ 0
(U1.R5)GPIO-0.26
Text HLabel 7150 5150 2    50   Input ~ 0
(U1.T5)
Text HLabel 7150 5250 2    50   Input ~ 0
(U1.P6)
Text HLabel 7150 5350 2    50   Input ~ 0
(U1.R6)
Text HLabel 7150 5450 2    50   Input ~ 0
(U1.T6)PRU-0.14
Text HLabel 7150 5550 2    50   Input ~ 0
(U1.P7)PRU-0.15
$Comp
L ValenBoard-rescue:OSD3358-512M-BSM-PocketBeagle U?
U 3 1 602DF7E2
P 4200 6250
AR Path="/602DF7E2" Ref="U?"  Part="3" 
AR Path="/602D1F90/602DF7E2" Ref="U?"  Part="3" 
F 0 "U?" H 4200 11250 39  0000 L BNN
F 1 "OSD3358-512M-BSM" H 4200 6150 39  0000 L BNN
F 2 "PocketBeagle.pretty:OSD335X-BGA-256" H 4190 6040 65  0001 L TNN
F 3 "" H 4200 6250 60  0001 C CNN
	3    4200 6250
	1    0    0    -1  
$EndComp
$EndSCHEMATC
