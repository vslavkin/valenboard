EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 9
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	6250 3400 6250 3800
Wire Wire Line
	6150 3900 6350 3900
Wire Wire Line
	6150 3800 6150 3900
Wire Wire Line
	6250 4100 6350 4100
Wire Wire Line
	6250 4600 6250 4100
Wire Wire Line
	6050 4600 6250 4600
Wire Wire Line
	6150 4000 6350 4000
Wire Wire Line
	6150 4200 6150 4000
Wire Wire Line
	6050 4200 6150 4200
Connection ~ 6850 4100
Connection ~ 6850 4000
Connection ~ 6850 3900
Wire Wire Line
	6750 4100 6850 4100
Wire Wire Line
	6750 4000 6850 4000
Wire Wire Line
	6750 3900 6850 3900
Wire Wire Line
	6850 3800 6750 3800
Wire Wire Line
	6850 4100 6850 4200
Wire Wire Line
	6850 4000 6850 4100
Wire Wire Line
	6850 3900 6850 4000
Wire Wire Line
	6850 3800 6850 3900
Wire Wire Line
	4850 3800 5750 3800
Wire Wire Line
	4850 4600 5750 4600
Wire Wire Line
	4850 3400 5750 3400
$Comp
L ValenBoard-rescue:LED-_INCH_-0603-PocketBeagle USR?
U 1 1 60394FD0
P 5850 4200
F 0 "USR?" V 6021 4150 31  0000 C CNN
F 1 "LTST-C191TBKT" V 5959 4150 31  0000 C CNN
F 2 "PocketBeagle.pretty:0603-LED" H 5840 3990 65  0001 L TNN
F 3 "" H 5850 4200 60  0001 C CNN
	1    5850 4200
	0    -1   -1   0   
$EndComp
$Comp
L ValenBoard-rescue:LED-_INCH_-0603-PocketBeagle USR?
U 1 1 60394FDC
P 5850 3400
F 0 "USR?" V 6021 3350 31  0000 C CNN
F 1 "LTST-C191TBKT" V 5959 3350 31  0000 C CNN
F 2 "PocketBeagle.pretty:0603-LED" H 5840 3190 65  0001 L TNN
F 3 "" H 5850 3400 60  0001 C CNN
	1    5850 3400
	0    -1   -1   0   
$EndComp
$Comp
L ValenBoard-rescue:RESISTOR.NETWORK-_4_-PocketBeagle R?
U 1 1 60394FE2
P 6550 4000
F 0 "R?" H 6401 4375 40  0000 L BNN
F 1 "1k" H 6401 4325 40  0000 L BNN
F 2 "PocketBeagle.pretty:RN0804" H 6540 3790 65  0001 L TNN
F 3 "" H 6550 4000 60  0001 C CNN
	1    6550 4000
	1    0    0    -1  
$EndComp
$Comp
L ValenBoard-rescue:GND-PocketBeagle #GND?
U 1 1 60394FE8
P 6850 4300
F 0 "#GND?" H 6750 4100 70  0001 L BNN
F 1 "GND" H 6850 4241 31  0000 C CNN
F 2 "" H 6850 4300 60  0001 C CNN
F 3 "" H 6850 4300 60  0001 C CNN
	1    6850 4300
	1    0    0    -1  
$EndComp
Text Notes 4850 1950 0    254  ~ 0
USER LEDs
Text HLabel 4850 3400 0    50   Input ~ 0
(U1.P13)USR0
Text HLabel 4850 3800 0    50   Input ~ 0
(U1.T14)USR1
Wire Wire Line
	4850 4200 5750 4200
Text HLabel 4850 4200 0    50   Input ~ 0
(U1.R14)USR2
Text HLabel 4850 4600 0    50   Input ~ 0
(U1.P14)USR3
$Comp
L ValenBoard-rescue:LED-_INCH_-0603-PocketBeagle USR?
U 1 1 60394FCA
P 5850 4600
F 0 "USR?" V 6021 4550 31  0000 C CNN
F 1 "LTST-C191TBKT" V 5959 4550 31  0000 C CNN
F 2 "PocketBeagle.pretty:0603-LED" H 5840 4390 65  0001 L TNN
F 3 "" H 5850 4600 60  0001 C CNN
	1    5850 4600
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6050 3400 6250 3400
$Comp
L ValenBoard-rescue:LED-_INCH_-0603-PocketBeagle USR?
U 1 1 60394FD6
P 5850 3800
F 0 "USR?" V 6021 3750 31  0000 C CNN
F 1 "LTST-C191TBKT" V 5959 3750 31  0000 C CNN
F 2 "PocketBeagle.pretty:0603-LED" H 5840 3590 65  0001 L TNN
F 3 "" H 5850 3800 60  0001 C CNN
	1    5850 3800
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6050 3800 6150 3800
Wire Wire Line
	6250 3800 6350 3800
$EndSCHEMATC
