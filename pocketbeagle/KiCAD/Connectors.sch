EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 9
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L ValenBoard-rescue:GND-PocketBeagle #GND?
U 1 1 5FBDC7C9
P 5050 3850
AR Path="/5FBDC7C9" Ref="#GND?"  Part="1" 
AR Path="/5FD9CD97/5FBDC7C9" Ref="#GND?"  Part="1" 
F 0 "#GND?" H 4950 3750 70  0001 L BNN
F 1 "GND" V 5050 3821 31  0000 R CNN
F 2 "" H 5050 3850 60  0001 C CNN
F 3 "" H 5050 3850 60  0001 C CNN
	1    5050 3850
	0    -1   -1   0   
$EndComp
$Comp
L ValenBoard-rescue:GND-PocketBeagle #GND?
U 1 1 5FBDC7CF
P 5050 4150
AR Path="/5FBDC7CF" Ref="#GND?"  Part="1" 
AR Path="/5FD9CD97/5FBDC7CF" Ref="#GND?"  Part="1" 
F 0 "#GND?" H 4950 4050 70  0001 L BNN
F 1 "GND" V 5050 4121 31  0000 R CNN
F 2 "" H 5050 4150 60  0001 C CNN
F 3 "" H 5050 4150 60  0001 C CNN
	1    5050 4150
	0    -1   -1   0   
$EndComp
$Comp
L ValenBoard-rescue:GND-PocketBeagle #GND?
U 1 1 5FBDC7D5
P 7300 3900
AR Path="/5FBDC7D5" Ref="#GND?"  Part="1" 
AR Path="/5FD9CD97/5FBDC7D5" Ref="#GND?"  Part="1" 
F 0 "#GND?" H 7201 3800 70  0001 L BNN
F 1 "GND" V 7300 3871 31  0000 R CNN
F 2 "" H 7300 3900 60  0001 C CNN
F 3 "" H 7300 3900 60  0001 C CNN
	1    7300 3900
	0    1    1    0   
$EndComp
$Comp
L ValenBoard-rescue:GND-PocketBeagle #GND?
U 1 1 5FBDC7DB
P 7175 4200
AR Path="/5FBDC7DB" Ref="#GND?"  Part="1" 
AR Path="/5FD9CD97/5FBDC7DB" Ref="#GND?"  Part="1" 
F 0 "#GND?" H 7076 4100 70  0001 L BNN
F 1 "GND" V 7175 4171 31  0000 R CNN
F 2 "" H 7175 4200 60  0001 C CNN
F 3 "" H 7175 4200 60  0001 C CNN
	1    7175 4200
	0    1    1    0   
$EndComp
$Comp
L ValenBoard-rescue:SYS_5V-PocketBeagle #SUPPLY?
U 1 1 5FBDC7ED
P 7075 3800
AR Path="/5FBDC7ED" Ref="#SUPPLY?"  Part="1" 
AR Path="/5FD9CD97/5FBDC7ED" Ref="#SUPPLY?"  Part="1" 
F 0 "#SUPPLY?" H 7075 3960 70  0001 L BNN
F 1 "SYS_5V" V 7075 3928 31  0000 L CNN
F 2 "" H 7075 3800 60  0001 C CNN
F 3 "" H 7075 3800 60  0001 C CNN
	1    7075 3800
	0    -1   -1   0   
$EndComp
$Comp
L ValenBoard-rescue:SYS_5V-PocketBeagle #SUPPLY?
U 1 1 5FBDC7F3
P 5150 4250
AR Path="/5FBDC7F3" Ref="#SUPPLY?"  Part="1" 
AR Path="/5FD9CD97/5FBDC7F3" Ref="#SUPPLY?"  Part="1" 
F 0 "#SUPPLY?" H 5150 4410 70  0001 L BNN
F 1 "SYS_5V" V 5150 4378 31  0000 L CNN
F 2 "" H 5150 4250 60  0001 C CNN
F 3 "" H 5150 4250 60  0001 C CNN
	1    5150 4250
	0    1    1    0   
$EndComp
$Comp
L ValenBoard-rescue:USB_DC-PocketBeagle #SUPPLY?
U 1 1 5FBDC805
P 1600 3450
AR Path="/5FBDC805" Ref="#SUPPLY?"  Part="1" 
AR Path="/5FD9CD97/5FBDC805" Ref="#SUPPLY?"  Part="1" 
F 0 "#SUPPLY?" H 1600 3610 70  0001 L BNN
F 1 "USB_DC" V 1600 3578 31  0000 L CNN
F 2 "" H 1600 3450 60  0001 C CNN
F 3 "" H 1600 3450 60  0001 C CNN
	1    1600 3450
	0    -1   -1   0   
$EndComp
$Comp
L ValenBoard-rescue:VDD_3V3B-PocketBeagle #SUPPLY?
U 1 1 5FBDC80B
P 7350 4300
AR Path="/5FBDC80B" Ref="#SUPPLY?"  Part="1" 
AR Path="/5FD9CD97/5FBDC80B" Ref="#SUPPLY?"  Part="1" 
F 0 "#SUPPLY?" H 7350 4460 70  0001 L BNN
F 1 "VDD_3V3B" V 7350 4428 31  0000 L CNN
F 2 "" H 7350 4300 60  0001 C CNN
F 3 "" H 7350 4300 60  0001 C CNN
	1    7350 4300
	0    -1   -1   0   
$EndComp
Wire Notes Line
	3750 3850 4950 3850
Wire Notes Line
	3750 4150 4950 4150
Text Notes 4150 1650 0    394  ~ 0
Connectors
Text HLabel 3950 4850 2    47   Input ~ 0
(U1.A1)PWM0A
Text HLabel 3950 4750 2    47   Input ~ 0
(U1.R5)GPIO-0.26
Text HLabel 3950 4650 2    47   Input ~ 0
(U1.A12)UART0.RX
Text HLabel 3950 4550 2    47   Input ~ 0
(U1.B12)UART0.TX
Text HLabel 3950 4450 2    47   Input ~ 0
(U1.A10)I2C2.SCL
Text HLabel 3950 4350 2    47   Input ~ 0
(U1.B10)I2C2.SDA
Text HLabel 3950 4050 2    47   Input ~ 0
(U1.B4)PRU-0.16
Text HLabel 3950 3950 2    47   Input ~ 0
(U1.B7)VREF+
Text HLabel 3950 3650 2    47   Output ~ 0
(U1.B14)SPI0.MOSI
Text HLabel 3950 3550 2    47   Input ~ 0
(U1.B13)SPI0.MISO
Text HLabel 3950 3450 2    47   Input ~ 0
(U1.A13)SPI0.CLK
Text HLabel 3950 3350 2    47   Input ~ 0
(U1.A14)SPI0.CS
Text HLabel 3950 3250 2    47   Input ~ 0
(U1.E1)
Text HLabel 3950 3150 2    47   Input ~ 0
(U1.F2)AIN6~3.3V
Text HLabel 1800 3250 0    47   Input ~ 0
(U1.M14)USB1.DRVVBUS
$Comp
L ValenBoard-rescue:VDD_5V-PocketBeagle #SUPPLY?
U 1 1 5FBDC7F9
P 1700 3150
AR Path="/5FBDC7F9" Ref="#SUPPLY?"  Part="1" 
AR Path="/5FD9CD97/5FBDC7F9" Ref="#SUPPLY?"  Part="1" 
F 0 "#SUPPLY?" H 1700 3310 70  0001 L BNN
F 1 "VDD_5V" V 1700 3278 39  0000 L CNN
F 2 "" H 1700 3150 60  0001 C CNN
F 3 "" H 1700 3150 60  0001 C CNN
	1    1700 3150
	0    -1   -1   0   
$EndComp
Text HLabel 1800 3350 0    47   Input ~ 0
(U1.M15)USB1.VBUS
Text HLabel 1800 3550 0    47   Input ~ 0
(U1.L16)USB1.D-
Text HLabel 1800 3650 0    47   Input ~ 0
(U1.L15)USB1.D+
Text HLabel 1800 3750 0    47   Input ~ 0
(U1.L14)USB1.ID
Wire Wire Line
	1800 3550 3300 3550
Wire Wire Line
	3950 3450 3750 3450
Wire Wire Line
	1700 3450 3300 3450
Wire Wire Line
	3950 3350 3750 3350
Wire Wire Line
	1800 3350 3300 3350
Wire Wire Line
	3750 3250 3950 3250
Wire Wire Line
	3950 4850 3750 4850
Wire Wire Line
	1800 4850 3300 4850
Wire Wire Line
	3950 4750 3750 4750
Wire Wire Line
	1800 4750 3300 4750
Wire Wire Line
	3950 4650 3750 4650
Wire Wire Line
	1800 4650 3300 4650
Wire Wire Line
	3950 4550 3750 4550
Wire Wire Line
	3300 3250 1800 3250
Wire Wire Line
	1800 4550 3300 4550
Wire Wire Line
	3950 4450 3750 4450
Wire Wire Line
	3300 4450 1800 4450
Wire Wire Line
	3950 4350 3750 4350
Wire Wire Line
	1800 4350 3300 4350
Wire Wire Line
	1800 4250 3300 4250
Wire Wire Line
	3300 4150 1800 4150
Wire Wire Line
	3950 4050 3750 4050
Wire Wire Line
	3950 3150 3750 3150
Wire Wire Line
	1800 4050 3300 4050
Wire Wire Line
	3950 3950 3750 3950
Wire Wire Line
	1800 3750 3300 3750
Wire Wire Line
	3950 3650 3750 3650
Wire Wire Line
	1800 3650 3300 3650
Wire Wire Line
	3950 3550 3750 3550
Wire Wire Line
	3300 3150 1800 3150
Wire Wire Line
	1800 3950 3300 3950
$Comp
L ValenBoard-rescue:GND-PocketBeagle #GND?
U 1 1 5FBDC7C3
P 1100 3850
AR Path="/5FBDC7C3" Ref="#GND?"  Part="1" 
AR Path="/5FD9CD97/5FBDC7C3" Ref="#GND?"  Part="1" 
F 0 "#GND?" H 1001 3750 70  0001 L BNN
F 1 "GND" V 1100 3821 31  0000 R CNN
F 2 "" H 1100 3850 60  0001 C CNN
F 3 "" H 1100 3850 60  0001 C CNN
	1    1100 3850
	0    1    1    0   
$EndComp
Wire Wire Line
	1200 3850 3300 3850
Text HLabel 1800 3950 0    47   Input ~ 0
(U1.B9)VREF-
Text HLabel 1800 4050 0    47   Input ~ 0
(U1.A8)AIN0~1.8V
Text HLabel 1800 4150 0    47   Input ~ 0
(U1.B8)AIN1~1.8V
Text HLabel 1800 4250 0    47   Input ~ 0
(U1.B6)AIN2~1.8V
Text HLabel 1800 4350 0    47   Input ~ 0
(U1.C6)AIN3~1.8V
Text HLabel 1800 4450 0    47   Input ~ 0
(U1.C7)AIN4~1.8V
Text HLabel 1800 4550 0    47   Input ~ 0
(U1.C4)PRU-0.7
Text HLabel 1800 4650 0    47   Input ~ 0
(U1.A3)PRU-0.4
Text HLabel 1800 4750 0    47   Input ~ 0
(U1.A2)PRU-0.1
Text HLabel 1800 4850 0    47   Input ~ 0
(U1.F1)
Text HLabel 8100 4900 0    47   Input ~ 0
(U1.F3)AIN5~3.3V
Text HLabel 8100 4800 0    47   Input ~ 0
(U1.R6)
Text HLabel 8100 4700 0    47   Input ~ 0
(U1.A4)SPI1.CS
Wire Wire Line
	9950 3200 9650 3200
Text HLabel 8100 4600 0    47   Input ~ 0
(U1.C5)SPI1.CLK
Text HLabel 8100 4500 0    47   Input ~ 0
(U1.C12)SPI1.MISO
Text HLabel 8100 4400 0    47   Input ~ 0
(U1.C13)SPI1.MOSI
Wire Wire Line
	9950 3300 9650 3300
Wire Wire Line
	9950 3400 9650 3400
Wire Wire Line
	9950 3700 9650 3700
Wire Wire Line
	9950 3800 9650 3800
Wire Wire Line
	9950 4100 9650 4100
Wire Wire Line
	9950 4200 9650 4200
Wire Wire Line
	9950 4300 9650 4300
Wire Wire Line
	9650 4400 9950 4400
Wire Wire Line
	9950 4600 9650 4600
Wire Wire Line
	9950 4700 9650 4700
Wire Wire Line
	9950 4800 9650 4800
Wire Wire Line
	9950 4900 9650 4900
Wire Wire Line
	5050 3750 3750 3750
Wire Wire Line
	4950 3850 3750 3850
Wire Wire Line
	4950 4150 3750 4150
Wire Wire Line
	5050 4250 3750 4250
Wire Wire Line
	8100 4800 9200 4800
Wire Wire Line
	8100 4900 9200 4900
Wire Wire Line
	8100 4700 9200 4700
Text HLabel 8100 4100 0    47   Input ~ 0
(U1.T5)
Text HLabel 8100 4000 0    47   Input ~ 0
(U1.T7)
Text HLabel 8100 3700 0    47   Input ~ 0
(U1.A11)I2C1.SDA
Text HLabel 8100 3600 0    47   Input ~ 0
(U1.B11)I2C1.SCL
Text HLabel 8100 3500 0    47   Input ~ 0
(U1.R16)UART4.TX
Text HLabel 8100 3400 0    47   Input ~ 0
(U1.P15)UART4.RX
Text HLabel 8100 3300 0    47   Input ~ 0
(U1.P5)GPIO-0.23
Text HLabel 8100 3200 0    47   Input ~ 0
(U1.P12)PWM1A
Text HLabel 9950 3200 2    47   Input ~ 0
(U1.T16)
Text HLabel 9950 3300 2    47   Input ~ 0
(U1.R15)
Text HLabel 9950 3400 2    47   Input ~ 0
(U1.T15)
Text HLabel 9950 3500 2    47   Input ~ 0
(U1.N14)
Wire Wire Line
	8100 3700 9200 3700
Wire Wire Line
	8100 3600 9200 3600
Wire Wire Line
	8100 4600 9200 4600
Wire Wire Line
	8100 4500 9200 4500
Wire Wire Line
	8100 4400 9200 4400
Wire Wire Line
	8100 3300 9200 3300
Wire Wire Line
	8100 3400 9200 3400
Wire Wire Line
	9200 3500 8100 3500
Wire Wire Line
	9200 4100 8100 4100
Wire Wire Line
	9200 4000 8100 4000
Wire Wire Line
	7400 3900 9200 3900
Wire Wire Line
	7275 4200 9200 4200
Wire Wire Line
	7175 3800 9200 3800
Wire Wire Line
	7450 4300 9200 4300
Wire Wire Line
	9950 4000 9650 4000
Wire Wire Line
	9950 3900 9650 3900
Wire Wire Line
	9950 3600 9650 3600
Wire Wire Line
	9950 3500 9650 3500
Wire Wire Line
	8100 3200 9200 3200
Text HLabel 9950 3600 2    47   Input ~ 0
(U1.R13)
Text HLabel 9950 3700 2    47   Input ~ 0
(U1.T11)PWR.BTN
Text HLabel 9950 3800 2    47   Input ~ 0
VIN.BAT
Text HLabel 9950 3900 2    47   Input ~ 0
BAT.TEMP
Text HLabel 9950 4000 2    47   Input ~ 0
(U1.P7)PRU-0.15
Text HLabel 9950 4100 2    47   Input ~ 0
(U1.R7)
Text HLabel 9950 4200 2    47   Input ~ 0
(U1.T6)PRU-0.14
Text HLabel 9950 4300 2    47   Input ~ 0
(U1.P6)
Text HLabel 9950 4400 2    47   Input ~ 0
(U1.R11)RESET#
Text HLabel 9950 4500 2    47   Input ~ 0
(U1.C3)PRU-0.6
Text HLabel 9950 4600 2    47   Input ~ 0
(U1.B1)PRU-0.3
Text HLabel 9950 4700 2    47   Input ~ 0
(U1.B2)PRU-0.2
Text HLabel 9950 4800 2    47   Input ~ 0
(U1.B3)PRU-0.5
Text HLabel 9950 4900 2    47   Input ~ 0
(U1.N13)AIN7~1.8V
$Comp
L ValenBoard-rescue:VDD_3V3B-PocketBeagle #SUPPLY?
U 1 1 5FBDC7FF
P 5150 3750
AR Path="/5FBDC7FF" Ref="#SUPPLY?"  Part="1" 
AR Path="/5FD9CD97/5FBDC7FF" Ref="#SUPPLY?"  Part="1" 
F 0 "#SUPPLY?" H 5150 3910 70  0001 L BNN
F 1 "VDD_3V3B" V 5150 3878 31  0000 L CNN
F 2 "" H 5150 3750 60  0001 C CNN
F 3 "" H 5150 3750 60  0001 C CNN
	1    5150 3750
	0    1    1    0   
$EndComp
$Comp
L ValenBoard-rescue:2X18-_INCH_-0.1-TH-_35MIL-DIA_-W_O-SILK-PocketBeagle P?
U 1 1 5FBDC7E1
P 3500 4050
AR Path="/5FBDC7E1" Ref="P?"  Part="1" 
AR Path="/5FD9CD97/5FBDC7E1" Ref="P?"  Part="1" 
F 0 "P?" H 3500 5200 70  0000 L BNN
F 1 "~" H 3490 3940 65  0001 L TNN
F 2 "PocketBeagle.pretty:TH-2X18-(35MIL-DIA)-W_O-SILK" H 3490 3840 65  0001 L TNN
F 3 "" H 3500 4050 60  0001 C CNN
	1    3500 4050
	1    0    0    -1  
$EndComp
$Comp
L ValenBoard-rescue:2X18-_INCH_-0.1-TH-_35MIL-DIA_-W_O-SILK-PocketBeagle P?
U 1 1 5FBDC7E7
P 9400 4100
AR Path="/5FBDC7E7" Ref="P?"  Part="1" 
AR Path="/5FD9CD97/5FBDC7E7" Ref="P?"  Part="1" 
F 0 "P?" H 9400 5200 70  0000 L BNN
F 1 "~" H 9390 3990 65  0001 L TNN
F 2 "PocketBeagle.pretty:TH-2X18-(35MIL-DIA)-W_O-SILK" H 9390 3890 65  0001 L TNN
F 3 "" H 9400 4100 60  0001 C CNN
	1    9400 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	9950 4500 9650 4500
$EndSCHEMATC
