EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 9
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 4400 1900 0    254  ~ 0
USB Device
$Comp
L ValenBoard-rescue:CAPACITOR-_INCH_-0402-PocketBeagle C?
U 1 1 600F5244
P 6800 4700
AR Path="/600F5244" Ref="C?"  Part="1" 
AR Path="/600655CF/600F5244" Ref="C?"  Part="1" 
F 0 "C?" H 6918 4731 31  0000 L CNN
F 1 ".1uF, 6.3V" H 6918 4669 31  0000 L CNN
F 2 "PocketBeagle.pretty:0402" H 6790 4490 65  0001 L TNN
F 3 "" H 6800 4700 60  0001 C CNN
	1    6800 4700
	1    0    0    -1  
$EndComp
$Comp
L ValenBoard-rescue:FB-_INCH_-0603-PocketBeagle FB?
U 1 1 600F524A
P 5300 4000
AR Path="/600F524A" Ref="FB?"  Part="1" 
AR Path="/600655CF/600F524A" Ref="FB?"  Part="1" 
F 0 "FB?" H 5300 4109 31  0000 C CNN
F 1 "~" H 5290 3890 65  0001 L TNN
F 2 "PocketBeagle.pretty:0603" H 5290 3790 65  0001 L TNN
F 3 "" H 5300 4000 60  0001 C CNN
	1    5300 4000
	1    0    0    -1  
$EndComp
$Comp
L ValenBoard-rescue:FB-_INCH_-0603-PocketBeagle FB?
U 1 1 600F5250
P 4600 5000
AR Path="/600F5250" Ref="FB?"  Part="1" 
AR Path="/600655CF/600F5250" Ref="FB?"  Part="1" 
F 0 "FB?" H 4600 5109 31  0000 C CNN
F 1 "~" H 4590 4890 65  0001 L TNN
F 2 "PocketBeagle.pretty:0603" H 4590 4790 65  0001 L TNN
F 3 "" H 4600 5000 60  0001 C CNN
	1    4600 5000
	1    0    0    -1  
$EndComp
$Comp
L ValenBoard-rescue:GND-PocketBeagle #GND?
U 1 1 600F5256
P 5100 5200
AR Path="/600F5256" Ref="#GND?"  Part="1" 
AR Path="/600655CF/600F5256" Ref="#GND?"  Part="1" 
F 0 "#GND?" H 5000 5100 70  0001 L BNN
F 1 "GND" H 5100 5141 31  0000 C CNN
F 2 "" H 5100 5200 60  0001 C CNN
F 3 "" H 5100 5200 60  0001 C CNN
	1    5100 5200
	1    0    0    -1  
$EndComp
$Comp
L ValenBoard-rescue:GND-PocketBeagle #GND?
U 1 1 600F525C
P 6800 4900
AR Path="/600F525C" Ref="#GND?"  Part="1" 
AR Path="/600655CF/600F525C" Ref="#GND?"  Part="1" 
F 0 "#GND?" H 6700 4800 70  0001 L BNN
F 1 "GND" H 6800 4841 31  0000 C CNN
F 2 "" H 6800 4900 60  0001 C CNN
F 3 "" H 6800 4900 60  0001 C CNN
	1    6800 4900
	1    0    0    -1  
$EndComp
$Comp
L ValenBoard-rescue:USB_DC-PocketBeagle #SUPPLY?
U 1 1 600F5262
P 6800 3800
AR Path="/600F5262" Ref="#SUPPLY?"  Part="1" 
AR Path="/600655CF/600F5262" Ref="#SUPPLY?"  Part="1" 
F 0 "#SUPPLY?" H 6800 3960 70  0001 L BNN
F 1 "USB_DC" H 6800 3959 31  0000 C CNN
F 2 "" H 6800 3800 60  0001 C CNN
F 3 "" H 6800 3800 60  0001 C CNN
	1    6800 3800
	1    0    0    -1  
$EndComp
$Comp
L ValenBoard-rescue:TPD4S012DRYR-PocketBeagle U?
U 1 1 600F526E
P 6200 4700
AR Path="/600F526E" Ref="U?"  Part="1" 
AR Path="/600655CF/600F526E" Ref="U?"  Part="1" 
F 0 "U?" H 6200 5028 40  0000 C CNN
F 1 "TPD4S012DRY" H 6200 4959 31  0000 C CNN
F 2 "PocketBeagle.pretty:SON-6(TI)-DRY" H 6190 4490 65  0001 L TNN
F 3 "" H 6200 4700 60  0001 C CNN
	1    6200 4700
	1    0    0    -1  
$EndComp
NoConn ~ 6600 4700
Wire Wire Line
	5000 4300 5300 4300
Wire Wire Line
	5300 4300 5600 4300
Wire Wire Line
	5800 4800 5300 4800
Wire Wire Line
	5300 4800 5300 4300
Connection ~ 5300 4300
Wire Wire Line
	5000 4200 5500 4200
Wire Wire Line
	5500 4200 5600 4200
Wire Wire Line
	5800 4600 5500 4600
Wire Wire Line
	5500 4600 5500 4200
Connection ~ 5500 4200
Wire Wire Line
	5000 4100 5400 4100
Wire Wire Line
	5400 4100 5600 4100
Wire Wire Line
	5800 4700 5400 4700
Wire Wire Line
	5400 4700 5400 4100
Connection ~ 5400 4100
Wire Wire Line
	4100 3900 4100 4000
Wire Wire Line
	4100 4000 4100 4100
Wire Wire Line
	4100 4100 4100 4200
Wire Wire Line
	4100 4200 4100 4300
Wire Wire Line
	4100 4300 4100 4400
Wire Wire Line
	4100 4400 4100 5000
Wire Wire Line
	4400 5000 4100 5000
Connection ~ 4100 4400
Connection ~ 4100 4300
Connection ~ 4100 4200
Connection ~ 4100 4100
Connection ~ 4100 4000
Connection ~ 4100 5000
Wire Wire Line
	5000 4400 5100 4400
Wire Wire Line
	5100 4400 5100 5000
Wire Wire Line
	5100 5000 5100 5100
Wire Wire Line
	4800 5000 5100 5000
Connection ~ 5100 5000
Wire Wire Line
	6600 4800 6800 4800
Connection ~ 6800 4800
Wire Wire Line
	6800 4600 6600 4600
Wire Wire Line
	6800 3900 6800 4000
Wire Wire Line
	6800 4000 6800 4600
Wire Wire Line
	6800 4000 5500 4000
Connection ~ 6800 4600
Connection ~ 6800 4000
Wire Wire Line
	5000 4000 5100 4000
Text GLabel 5100 5100 1    10   UnSpc ~ 0
GND
Text GLabel 5000 4400 2    10   UnSpc ~ 0
GND
Text GLabel 6800 4800 1    10   UnSpc ~ 0
GND
Text GLabel 6600 4800 2    10   UnSpc ~ 0
GND
Text GLabel 6800 4800 0    10   UnSpc ~ 0
GND
Text GLabel 6800 3900 3    10   UnSpc ~ 0
USB_DC
Text GLabel 6600 4600 2    10   UnSpc ~ 0
USB_DC
Text GLabel 5500 4000 2    10   UnSpc ~ 0
USB_DC
Text GLabel 6800 4600 0    10   UnSpc ~ 0
USB_DC
Text GLabel 5000 4000 2    10   UnSpc ~ 0
VBUS.RAW
Text GLabel 5100 4000 0    10   UnSpc ~ 0
VBUS.RAW
Wire Wire Line
	4100 4400 4200 4400
Wire Wire Line
	4200 4300 4100 4300
Wire Wire Line
	4200 4200 4100 4200
Wire Wire Line
	4200 4100 4100 4100
Wire Wire Line
	4200 4000 4100 4000
Wire Wire Line
	4200 3900 4100 3900
$Comp
L ValenBoard-rescue:10118192-0001LF-PocketBeagle X?
U 1 1 600F5274
P 4700 4200
AR Path="/600F5274" Ref="X?"  Part="1" 
AR Path="/600655CF/600F5274" Ref="X?"  Part="1" 
F 0 "X?" H 4800 4750 70  0000 C CNN
F 1 "10118192-0001LF" H 4800 4658 31  0000 C CNN
F 2 "PocketBeagle.pretty:10118192-0001LF(MICRO-USB-B-AMPHENOL-FCI)" H 4690 3990 65  0001 L TNN
F 3 "" H 4700 4200 60  0001 C CNN
	1    4700 4200
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4100 5000 4100 5100
$Comp
L ValenBoard-rescue:CGND-PocketBeagle #SUPPLY?
U 1 1 600F5268
P 4100 5200
AR Path="/600F5268" Ref="#SUPPLY?"  Part="1" 
AR Path="/600655CF/600F5268" Ref="#SUPPLY?"  Part="1" 
F 0 "#SUPPLY?" H 4100 5060 70  0001 L BNN
F 1 "CGND" H 4075 5041 31  0000 C CNN
F 2 "" H 4100 5200 60  0001 C CNN
F 3 "" H 4100 5200 60  0001 C CNN
	1    4100 5200
	1    0    0    -1  
$EndComp
Text HLabel 5600 4300 2    50   Input ~ 0
(U1.R14)USR2
Text HLabel 5600 4200 2    50   Input ~ 0
(U1.K15)USBC.D+
Text HLabel 5600 4100 2    50   Input ~ 0
(U1.K16)USBC.D-
$EndSCHEMATC
