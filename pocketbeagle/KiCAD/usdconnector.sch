EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 9
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 4450 1600 0    254  ~ 0
uSD Connector
$Comp
L ValenBoard-rescue:CAPACITOR-_INCH_-0402-PocketBeagle C?
U 1 1 604B9424
P 6750 3750
AR Path="/604B9424" Ref="C?"  Part="1" 
AR Path="/604AFD96/604B9424" Ref="C?"  Part="1" 
F 0 "C?" H 6868 3781 31  0000 L CNN
F 1 "0.1uF, 6.3V" H 6868 3719 31  0000 L CNN
F 2 "PocketBeagle.pretty:0402" H 6740 3540 65  0001 L TNN
F 3 "" H 6750 3750 60  0001 C CNN
	1    6750 3750
	1    0    0    -1  
$EndComp
$Comp
L ValenBoard-rescue:CAPACITOR-_INCH_-0603-PocketBeagle C?
U 1 1 604B942A
P 5950 3750
AR Path="/604B942A" Ref="C?"  Part="1" 
AR Path="/604AFD96/604B942A" Ref="C?"  Part="1" 
F 0 "C?" H 6068 3781 31  0000 L CNN
F 1 "10uF, 10V" H 6068 3719 31  0000 L CNN
F 2 "PocketBeagle.pretty:0603" H 5940 3540 65  0001 L TNN
F 3 "" H 5950 3750 60  0001 C CNN
	1    5950 3750
	1    0    0    -1  
$EndComp
$Comp
L ValenBoard-rescue:GND-PocketBeagle #GND?
U 1 1 604B9430
P 6350 4250
AR Path="/604B9430" Ref="#GND?"  Part="1" 
AR Path="/604AFD96/604B9430" Ref="#GND?"  Part="1" 
F 0 "#GND?" H 6250 4150 70  0001 L BNN
F 1 "GND" H 6350 4191 31  0000 C CNN
F 2 "" H 6350 4250 60  0001 C CNN
F 3 "" H 6350 4250 60  0001 C CNN
	1    6350 4250
	1    0    0    -1  
$EndComp
$Comp
L ValenBoard-rescue:RESISTOR.NETWORK-_4_-PocketBeagle R?
U 1 1 604B943C
P 4650 3850
AR Path="/604B943C" Ref="R?"  Part="1" 
AR Path="/604AFD96/604B943C" Ref="R?"  Part="1" 
F 0 "R?" V 4688 4177 40  0000 L CNN
F 1 "10k" V 4612 4177 40  0000 L CNN
F 2 "PocketBeagle.pretty:RN0804" H 4640 3640 65  0001 L TNN
F 3 "" H 4650 3850 60  0001 C CNN
	1    4650 3850
	0    -1   -1   0   
$EndComp
$Comp
L ValenBoard-rescue:RESISTOR.NETWORK-_4_-PocketBeagle R?
U 1 1 604B9442
P 5050 3850
AR Path="/604B9442" Ref="R?"  Part="1" 
AR Path="/604AFD96/604B9442" Ref="R?"  Part="1" 
F 0 "R?" V 5012 4178 40  0000 L CNN
F 1 "10k" V 5088 4178 40  0000 L CNN
F 2 "PocketBeagle.pretty:RN0804" H 5040 3640 65  0001 L TNN
F 3 "" H 5050 3850 60  0001 C CNN
	1    5050 3850
	0    1    1    0   
$EndComp
$Comp
L ValenBoard-rescue:VDD_3V3B-PocketBeagle #SUPPLY?
U 1 1 604B944E
P 2850 4850
AR Path="/604B944E" Ref="#SUPPLY?"  Part="1" 
AR Path="/604AFD96/604B944E" Ref="#SUPPLY?"  Part="1" 
F 0 "#SUPPLY?" H 2850 5150 70  0001 L BNN
F 1 "VDD_3V3B" V 2850 4978 31  0000 L CNN
F 2 "" H 2850 4850 60  0001 C CNN
F 3 "" H 2850 4850 60  0001 C CNN
	1    2850 4850
	0    -1   -1   0   
$EndComp
$Comp
L ValenBoard-rescue:VDD_3V3B-PocketBeagle #SUPPLY?
U 1 1 604B9454
P 6350 3350
AR Path="/604B9454" Ref="#SUPPLY?"  Part="1" 
AR Path="/604AFD96/604B9454" Ref="#SUPPLY?"  Part="1" 
F 0 "#SUPPLY?" H 6350 3510 70  0001 L BNN
F 1 "VDD_3V3B" H 6350 3509 31  0000 C CNN
F 2 "" H 6350 3350 60  0001 C CNN
F 3 "" H 6350 3350 60  0001 C CNN
	1    6350 3350
	1    0    0    -1  
$EndComp
$Comp
L ValenBoard-rescue:CGND-PocketBeagle #SUPPLY?
U 1 1 604B945A
P 8350 5450
AR Path="/604B945A" Ref="#SUPPLY?"  Part="1" 
AR Path="/604AFD96/604B945A" Ref="#SUPPLY?"  Part="1" 
F 0 "#SUPPLY?" H 8350 5200 70  0001 L BNN
F 1 "CGND" H 8150 5200 70  0000 L BNN
F 2 "" H 8350 5450 60  0001 C CNN
F 3 "" H 8350 5450 60  0001 C CNN
	1    8350 5450
	1    0    0    -1  
$EndComp
Wire Wire Line
	4950 4050 4950 5150
Wire Wire Line
	4750 4050 4750 4950
Wire Wire Line
	4650 4050 4650 4750
Wire Wire Line
	5150 4050 5150 5350
Wire Wire Line
	4550 4050 4550 4650
Wire Wire Line
	4450 4050 4450 4550
Wire Wire Line
	8350 4050 8350 5150
Wire Wire Line
	8350 5150 8350 5350
Wire Wire Line
	7450 5750 7550 5750
Wire Wire Line
	7550 5750 7750 5750
Wire Wire Line
	7750 5750 7750 5150
Wire Wire Line
	7750 5150 8350 5150
Connection ~ 8350 5150
Connection ~ 7550 5750
Wire Wire Line
	5950 4050 6350 4050
Wire Wire Line
	6350 4050 6750 4050
Wire Wire Line
	6350 4150 6350 4050
Wire Wire Line
	6750 4050 6750 3850
Wire Wire Line
	5950 4050 5950 3850
Connection ~ 6350 4050
Wire Wire Line
	5950 3650 5950 3550
Wire Wire Line
	5950 3550 6350 3550
Wire Wire Line
	6350 3550 6750 3550
Wire Wire Line
	6350 3450 6350 3550
Wire Wire Line
	6750 3550 6750 3650
Connection ~ 6350 3550
NoConn ~ 5250 3650
NoConn ~ 5250 4050
Wire Wire Line
	4450 4550 3350 4550
Connection ~ 4450 4550
Wire Wire Line
	4550 4650 3350 4650
Connection ~ 4550 4650
Wire Wire Line
	4650 4750 3350 4750
Connection ~ 4650 4750
Connection ~ 4550 3550
Connection ~ 4650 3550
Connection ~ 4950 3550
Connection ~ 5050 3550
Connection ~ 4850 3550
Wire Wire Line
	4450 3550 4450 3650
Wire Wire Line
	4650 3650 4650 3550
Wire Wire Line
	4750 3650 4750 3550
Wire Wire Line
	4950 3650 4950 3550
Wire Wire Line
	5050 3650 5050 3550
Wire Wire Line
	5150 3550 5150 3650
Wire Wire Line
	4850 3550 4850 3450
Wire Wire Line
	5050 3550 5150 3550
Wire Wire Line
	4950 3550 5050 3550
Wire Wire Line
	4850 3550 4950 3550
Wire Wire Line
	4550 3550 4650 3550
Wire Wire Line
	4450 3550 4550 3550
Wire Wire Line
	4550 3650 4550 3550
$Comp
L ValenBoard-rescue:VDD_3V3B-PocketBeagle #SUPPLY?
U 1 1 604B9448
P 4850 3350
AR Path="/604B9448" Ref="#SUPPLY?"  Part="1" 
AR Path="/604AFD96/604B9448" Ref="#SUPPLY?"  Part="1" 
F 0 "#SUPPLY?" H 4850 3510 70  0001 L BNN
F 1 "VDD_3V3B" H 4850 3509 31  0000 C CNN
F 2 "" H 4850 3350 60  0001 C CNN
F 3 "" H 4850 3350 60  0001 C CNN
	1    4850 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	7450 4050 7450 4250
Wire Wire Line
	7550 5650 7550 5750
Wire Wire Line
	7450 5650 7450 5750
Wire Wire Line
	5150 5350 6450 5350
Wire Wire Line
	5050 5250 6450 5250
Wire Wire Line
	2950 4850 6450 4850
Wire Wire Line
	4650 4750 6450 4750
Wire Wire Line
	4550 4650 6450 4650
Wire Wire Line
	4450 4550 6450 4550
$Comp
L ValenBoard-rescue:MICROSD-PocketBeagle X?
U 1 1 604B9460
P 7150 5150
AR Path="/604B9460" Ref="X?"  Part="1" 
AR Path="/604AFD96/604B9460" Ref="X?"  Part="1" 
F 0 "X?" H 7051 5400 70  0000 L BNN
F 1 "MICROSD" H 7051 5300 70  0000 L BNN
F 2 "PocketBeagle.pretty:MICROSD" H 7750 5350 70  0001 L BNN
F 3 "" H 7150 5150 60  0001 C CNN
	1    7150 5150
	1    0    0    -1  
$EndComp
$Comp
L ValenBoard-rescue:GND-PocketBeagle #GND?
U 1 1 604B9436
P 2600 5050
AR Path="/604B9436" Ref="#GND?"  Part="1" 
AR Path="/604AFD96/604B9436" Ref="#GND?"  Part="1" 
F 0 "#GND?" H 2500 4850 70  0001 L BNN
F 1 "GND" V 2600 5021 31  0000 R CNN
F 2 "" H 2600 5050 60  0001 C CNN
F 3 "" H 2600 5050 60  0001 C CNN
	1    2600 5050
	0    1    1    0   
$EndComp
Wire Wire Line
	2700 5050 6450 5050
Wire Wire Line
	4750 4950 4000 4950
Wire Wire Line
	4950 5150 6450 5150
Wire Wire Line
	5050 4050 5050 5250
Wire Wire Line
	4950 5150 3700 5150
Connection ~ 4950 5150
Wire Wire Line
	5050 5250 3700 5250
Connection ~ 5050 5250
Wire Wire Line
	5150 5350 3700 5350
Connection ~ 5150 5350
Text HLabel 3350 4550 0    50   Input ~ 0
(U1.C16)MMC0.D2
Text HLabel 3350 4650 0    50   Input ~ 0
(U1.C15)MMC0.D3
Text HLabel 3350 4750 0    50   Input ~ 0
(U1.B16)MMC0.CMD
Text HLabel 4000 4950 0    50   Input ~ 0
(U1.B15)MMC0.CLK
Connection ~ 4750 4950
Wire Wire Line
	4750 4950 6450 4950
Text HLabel 3700 5150 0    50   Input ~ 0
(U1.A16)MMC0.D0
Text HLabel 3700 5250 0    50   Input ~ 0
(U1.A15)MMC0.D1
Text HLabel 3700 5350 0    50   Input ~ 0
(U1.C14)MMC0.CD
Wire Wire Line
	7550 4050 8350 4050
Wire Wire Line
	7450 4050 7550 4050
Connection ~ 7550 4050
Wire Wire Line
	7550 4250 7550 4050
Wire Wire Line
	4650 3550 4750 3550
Wire Wire Line
	4750 3550 4850 3550
Connection ~ 4750 3550
$EndSCHEMATC
